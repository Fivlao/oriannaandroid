package com.anime.orianna

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


@Suppress("DEPRECATION")
class MainActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        val display = windowManager.defaultDisplay
//        setSize(display.width, display.height);
        run()
    }

    init {
        System.loadLibrary("OriannaApp")
    }
//    private external fun setSize(with: Int, height: Int): Unit
    private external fun run(): Unit
}