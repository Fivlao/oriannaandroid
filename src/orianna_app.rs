use std::os::raw::{c_char, c_int};
use std::ffi::{CString, CStr};
extern crate qmetaobject;
use qmetaobject::*;
#[macro_use] extern crate cstr;

#[no_mangle]
pub extern fn run() {
    let mut engine = QmlEngine::new();
    engine.load_data(r#"
    import QtQuick 2.6;
    import QtQuick.Window 2.0;

    Window {
        visible: true;
        Text {
            anchors.centerIn: parent;
            text: 'hello World';
        }
    }"#.into());
    engine.exec();
}


#[cfg(target_os="android")]
#[allow(non_snake_case)]
pub mod android {
    extern crate jni;

    use super::*;
    use self::jni::JNIEnv;
    use self::jni::objects::{JClass, JString};
    use self::jni::sys::{jstring};

    #[no_mangle]
    pub unsafe extern fn Java_MainActivity_run(env: JNIEnv, _: JClass) {
        run();
    }
}